package com.bbva.fiduciaria.pos.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CleanCache implements Runnable {
	/*
	 * executeClean: Método utilizado para controlar la ejecución de la  limpieza a
	 * la memoria del pos, se define un minuto para que se ejecute, es
	 * decir la llave de cifrado estará en memoria durante un minuto y luego
	 * se eliminará.
	 */
	public void executeClean() {
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
		Runnable task = new CleanCache();
		int delay = 1;
		scheduler.schedule(task, delay, TimeUnit.MINUTES);
	}

	@Override
	public void run() {
		POSUtils.ckey = null;
	}

}
