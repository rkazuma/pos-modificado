package com.bbva.fiduciaria.pos.interfaces;

import com.bbva.fiduciaria.pos.dto.DeviceInfoDTO;
import com.bbva.fiduciaria.pos.dto.IpMacDTO;
import com.bbva.fiduciaria.pos.dto.TokenCaptorDTO;
import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;

public interface BioMiniInterface
{
	public void init() throws SupremaDeviceException;

	public void capture() throws SupremaDeviceException;

	public byte[] getCapture() throws SupremaDeviceException;

	public byte[] getTemplate() throws SupremaDeviceException;

	public byte[] getEncryptedTemplate(byte[] cKey) throws SupremaDeviceException;

	public int getTemplateQuality() throws SupremaDeviceException;

	public void verify() throws SupremaDeviceException;
	
	public IpMacDTO getIpMac() throws SupremaDeviceException;	
	
	public TokenCaptorDTO generateKey() throws SupremaDeviceException;	
	
	public DeviceInfoDTO getInfoDevice() throws SupremaDeviceException;
	
}
