package com.bbva.fiduciaria.pos.rs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bbva.fiduciaria.pos.dto.DeviceInfoDTO;
import com.bbva.fiduciaria.pos.dto.IpMacDTO;
import com.bbva.fiduciaria.pos.dto.TokenCaptorDTO;
import com.bbva.fiduciaria.pos.exception.FiduciariaPOSException;
import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;

@Path("/captor-huellas")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public interface CaptorHuellaRS {
	@GET
	@Path("/iniciar")
	public void iniciar() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/capturar")
	public void capturar() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/template")
	public Response obtenerTemplate() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/calidad")
	public int obtenerCalidad() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Path("/verificar")
	public void verificarCaptura() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Produces("application/json")
	@Path("/obteneripmac")
	public IpMacDTO obtenerIpMac() throws FiduciariaPOSException, SupremaDeviceException;

	@GET
	@Produces("application/json")
	@Path("/obtenerllave")
	public TokenCaptorDTO obtenerllave() throws FiduciariaPOSException, SupremaDeviceException;
	
	@GET
	@Produces("application/json")
	@Path("/obtenerinfodisp")
	public DeviceInfoDTO obtenerInfoDisp() throws FiduciariaPOSException, SupremaDeviceException;

}
