package com.bbva.fiduciaria.pos.dto;

import java.io.Serializable;

public class RSExceptionDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	
	private String type;
	
	private String stacktrace;

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getStacktrace()
	{
		return stacktrace;
	}

	public void setStacktrace(String stacktrace)
	{
		this.stacktrace = stacktrace;
	}	
}