package com.bbva.fiduciaria.pos;

import com.bbva.fiduciaria.pos.util.POSUtils;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.bbva.fiduciaria.pos.config.AppConfig;
import com.bbva.fiduciaria.pos.filter.CORSFilter;
import com.bbva.fiduciaria.pos.filter.ValidarTokenFilter;

public class PosApp
{
	public static void main(final String[] args) throws Exception
	{
	    Integer serverPort = Integer.valueOf(POSUtils.getProperty("fiduciaria.pos.port"));
	    String contextPath = POSUtils.getProperty("fiduciaria.pos.context");

		Server server = new Server(serverPort);
		ServletContextHandler context = new ServletContextHandler();

		// Context config
		context.setContextPath(contextPath);
		context.addEventListener(new ContextLoaderListener());
		context.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
		context.setInitParameter("contextConfigLocation", AppConfig.class.getName());

		// CORS Filter
 		FilterHolder corsFilter = new FilterHolder(CORSFilter.class); 		
 		context.addFilter(corsFilter, "/*", null);
		
		// Character encoding filter
		FilterHolder characterEncodingFilter = new FilterHolder(CharacterEncodingFilter.class);
		characterEncodingFilter.setInitParameter("encoding", "UTF-8");
		characterEncodingFilter.setInitParameter("forceEncoding", "true");
		context.addFilter(characterEncodingFilter, "/*", null);
		
		// ValidarToken Filter
 		FilterHolder validarTokenFilter = new FilterHolder(ValidarTokenFilter.class);
 		context.addFilter(validarTokenFilter, "/*", null);
		 		
		// CXF Servlet
		ServletHolder servletHolder = new ServletHolder(new CXFServlet());
		context.addServlet(servletHolder, "/services/*");

		// Start
		server.setHandler(context);
		server.start();
		server.join();
	}
}
