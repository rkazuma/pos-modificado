package com.bbva.fiduciaria.pos.exception;

public class SupremaDeviceException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2823526933148798146L;

	public SupremaDeviceException()
	{
		super();
	}

	public SupremaDeviceException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public SupremaDeviceException(String message)
	{
		super(message);
	}

	public SupremaDeviceException(Throwable cause)
	{
		super(cause);
	}
}
