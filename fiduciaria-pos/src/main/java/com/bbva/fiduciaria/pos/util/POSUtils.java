package com.bbva.fiduciaria.pos.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Properties;
import java.util.Scanner;

import com.bbva.fiduciaria.pos.dto.IpMacDTO;
import com.bbva.fiduciaria.pos.exception.SupremaDeviceException;

public class POSUtils {

	public static final String TOKEN_CAPTOR_ATTRIBUTE_KEY = "TOKEN_CAPTOR_ATTRIBUTE";

	public static Properties properties;
	
	public static String ckey = "";

	public static String getProperty(String key) throws Exception {
		if (properties == null) {
			properties = new Properties();
			InputStream inputStream = POSUtils.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(inputStream);
		}

		return (String) properties.get(key);
	}
	public IpMacDTO getIpMac() throws SupremaDeviceException{
		InetAddress ip;
		IpMacDTO ipMacDTO = new IpMacDTO();
		try {
			ip = InetAddress.getLocalHost();
			ipMacDTO.setIp(ip.getHostAddress());			
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			ipMacDTO.setMac(sb.toString());
			ipMacDTO.setKey(ckey);
			System.out.print(getARPTable());
			return ipMacDTO;
			} catch (Exception e) {
			return null;
		}
	}
	@SuppressWarnings("resource")
	public String getARPTable(
			) throws IOException {        
		Scanner s = new Scanner(Runtime.getRuntime().exec("arp -a").getInputStream()).useDelimiter("\\A");
		//return s.findInLine("dinamica");
            return s.hasNext() ? s.next() : "";
 }
	
	public IpMacDTO getIpMacRouter() throws SupremaDeviceException{		
		InetAddress ip;
		IpMacDTO ipMacDTO = new IpMacDTO();
		try {
			ip = InetAddress.getLocalHost();
			ipMacDTO.setIp(ip.getHostAddress());			
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			byte[] mac = network.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			ipMacDTO.setMac(sb.toString());
			return ipMacDTO;
		} catch (Exception e) {
			return null;
		}
	}	
}
