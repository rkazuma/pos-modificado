package com.bbva.fiduciaria.pos.exception;

public class PlataformaException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String stacktrace;

	public PlataformaException(String message)
	{
		super(message);
	}
	
	public PlataformaException(String message, String stacktrace)
	{
		super(message);
		
		this.stacktrace = stacktrace;
	}

	public String getStacktrace()
	{
		return stacktrace;
	}

	public void setStacktrace(String stacktrace)
	{
		this.stacktrace = stacktrace;
	}
}	
