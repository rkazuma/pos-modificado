package com.bbva.fiduciaria.pos.dto;

import java.io.Serializable;

public class IpMacDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String ip;
	private String mac;
	private String key;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
}