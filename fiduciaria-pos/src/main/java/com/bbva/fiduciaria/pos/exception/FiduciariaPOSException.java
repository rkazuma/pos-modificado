package com.bbva.fiduciaria.pos.exception;

public class FiduciariaPOSException extends Exception {
	private static final long serialVersionUID = 1L;

	private String titulo;

	public FiduciariaPOSException(String titulo, String message) {
		super(message);
		this.titulo = titulo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
}
