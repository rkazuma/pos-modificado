package com.bbva.fiduciaria.pos.config;

import java.util.Arrays;

import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import com.bbva.fiduciaria.pos.rs.CaptorHuellaRS;
import com.bbva.fiduciaria.pos.util.RSExceptionMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Configuration
@ComponentScan(basePackages = "com.bbva.fiduciaria.pos")
public class AppConfig
{
	@Autowired
	@Qualifier("captorHuellaRS")
	private CaptorHuellaRS captorHuellaRS;
	
	@Bean(destroyMethod = "shutdown")
	public SpringBus cxf()
	{
		return new SpringBus();
	}

	@Bean
	@DependsOn("cxf")
	public Server jaxRsServer()
	{
		JAXRSServerFactoryBean factory = RuntimeDelegate.getInstance().createEndpoint(new Application(), JAXRSServerFactoryBean.class);
		factory.setServiceBeans(Arrays.<Object> asList(captorHuellaRS));
		factory.setAddress(factory.getAddress());
		factory.setProviders(Arrays.<Object> asList(new JacksonJsonProvider(), new RSExceptionMapper()));
		return factory.create();
	}
}
