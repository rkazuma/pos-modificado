package com.bbva.fiduciaria.pos.dto;

import java.util.Date;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TokenCaptorDTO {
	private String token;

	private String usuarioEkip;

	private Date fechaGeneracion;

	private String ckey;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsuarioEkip() {
		return usuarioEkip;
	}

	public void setUsuarioEkip(String usuarioEkip) {
		this.usuarioEkip = usuarioEkip;
	}

	public Date getFechaGeneracion()
	{
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public String getCkey() {
		return ckey;
	}

	public void setCkey(String ckey) {
		this.ckey = ckey;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(usuarioEkip).append(token).append(fechaGeneracion).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}

		if (!(obj instanceof TokenCaptorDTO)) {
			return false;
		}

		TokenCaptorDTO tokenCaptor = (TokenCaptorDTO) obj;

		return new EqualsBuilder().append(usuarioEkip, tokenCaptor.usuarioEkip).append(token, tokenCaptor.token).append(fechaGeneracion, tokenCaptor.fechaGeneracion).isEquals();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("token", token).append("usuarioEkip", usuarioEkip).append("fechaGeneracion", fechaGeneracion).append("ckey", ckey).toString();
	}
}