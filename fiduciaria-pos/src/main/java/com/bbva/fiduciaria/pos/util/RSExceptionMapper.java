package com.bbva.fiduciaria.pos.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.bbva.fiduciaria.pos.exception.PlataformaException;

public class RSExceptionMapper implements ExceptionMapper<Throwable>
{
	public Response toResponse(Throwable exception)
	{			
		Map<String, String> responseBody = new HashMap<String, String>();
		responseBody.put("message", exception.getMessage());
		
		if(exception instanceof PlataformaException) 
		{
			PlataformaException plataformaException = (PlataformaException) exception;
			
			if(plataformaException.getStacktrace() != null && !plataformaException.getStacktrace().isEmpty())
			{
				responseBody.put("stacktrace", plataformaException.getStacktrace());
			}
		}
		
		// Si no hay stacktrace seteola de la exception
		if(!responseBody.containsKey("stacktrace")) 
		{
			StringWriter swStackTrace = new StringWriter();
			exception.printStackTrace(new PrintWriter(swStackTrace));
			responseBody.put("stacktrace", swStackTrace.toString());
		}
		
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseBody).type(MediaType.APPLICATION_JSON).build();
	}

}
