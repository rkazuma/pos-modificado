package com.bbva.fiduciaria.pos.dto;

public class DeviceInfoDTO {

	private String DeviceName;
	private String DeviceSN;
	private String DeviceFWVersion;
	private String DeviceHWVersion;

	public String getDeviceName() {
		return DeviceName;
	}

	public void setDeviceName(String deviceName) {
		DeviceName = deviceName;
	}

	public String getDeviceSN() {
		return DeviceSN;
	}

	public void setDeviceSN(String deviceSN) {
		DeviceSN = deviceSN;
	}

	public String getDeviceFWVersion() {
		return DeviceFWVersion;
	}

	public void setDeviceFWVersion(String deviceFWVersion) {
		DeviceFWVersion = deviceFWVersion;
	}

	public String getDeviceHWVersion() {
		return DeviceHWVersion;
	}

	public void setDeviceHWVersion(String deviceHWVersion) {
		DeviceHWVersion = deviceHWVersion;
	}

}
